package ru.dolbak.yandexapi;

import android.os.Bundle;
import android.os.Message;
import android.os.Handler;

import java.io.IOException;


import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MyThread extends Thread {
    Handler handler;
    OkHttpClient client;
    String picture;

    public MyThread (Handler handler, String picture){
        this.handler = handler;
        client = new OkHttpClient();
        this.picture = picture;
    }

    @Override
    public void run(){

        final MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");

        super.run();

        HttpUrl.Builder strBuilder = HttpUrl.parse("https://vision.api.cloud.yandex.net/vision/v1/batchAnalyze").newBuilder();

        String json = "{\n" +
                "    \"folderId\": \"b1g9snh0l7i56pamb3iq\",\n" +
                "    \"analyze_specs\": [{\n" +
                "        \"content\": \"" + picture + "\",\n" +
                "        \"features\": [{\n" +
                "            \"type\": \"TEXT_DETECTION\",\n" +
                "            \"text_detection_config\": {\n" +
                "                \"language_codes\": [\"*\"]\n" +
                "            }\n" +
                "        }]\n" +
                "    }]\n" +
                "}";

        //strBuilder.addQueryParameter("content", json);

        RequestBody body = RequestBody.create(json, JSON);

        String url = strBuilder.build().toString();

        Request request = new Request.Builder().url(url)
                .post(body)
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", "Bearer t1.9euelZqLmZuLmsaSlIyOjJzIlZDIku3rnpWajJiPzJWUnouam87JyI-Ym5Hl8_cxJhte-e9ATXtd_t3z93FUGF7570BNe13-.Boa5RJG0-ZnCgcfoGm6RbeuvDruIqDhNUeHtqPIBWueNmPxEbpAV0pmnO3AJDceITtXvDArrKbsD5AC7_8r3BQ")
                .build();

        try {
            Response response = client.newCall(request).execute();

            String res = response.body().string();
            Message message = new Message();
            Bundle bundle = new Bundle();
            bundle.putString("body", res);
            message.setData(bundle);
            handler.sendMessage(message);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
